set beg 0
set skip 1
# set end -1
set end -1

# mol new PORE.dx

set prefix output/dna-70

set naLength 1
set IDs [mol new dna-70.psf ]
# set files [sortFileGlob $prefix.dcd] 
dcd $prefix.0.dcd skip $skip beg $beg end $end

source loadRigidBody.procs.tcl
set files [sortFileGlob $prefix.rb-traj] 
puts "loading $files"
set rbIDs [loadTrajectory $files [lindex $IDs 0] $skip $beg $end]
# loadTrajectoryRbFrame $files [lindex $IDs 0] $skip $beg $endo

foreach ID $IDs {
    set sel [atomselect $ID all]
    set beta ""
    foreach r [$sel get resid] {
	lappend beta [expr $r%10 + double($r)/10] 
    }
    $sel set beta $beta

    set sel [atomselect $ID "name P"]
    $sel set radius 2.5
    set sel [atomselect $ID "name B"]
    $sel set radius 2


    # mol color Beta
    mol color Name
    # mol color Index
    mol selection all
    mol selection "name P B"
    mol material AOChalky
    # mol representation VDW 3.000000 12.000000
    # mol representation VDW 0.8
    mol representation Licorice 1.3
    mol modrep 0 $ID
    # mol delrep 0 $ID

   # mol representation Licorice 0.6
   # mol addrep $ID
}

# mol color Beta
# mol color ColorID 0
mol color SegName
# mol color Molecule
mol selection "protein or nucleic"
mol material AOChalky
# mol material GlassBubble
# mol representation VDW 0.8
# mol representation QuickSurf

# mol representation VDW 2.000000 12.000000
# mol representation NewCartoon
mol representation QuickSurf 1.000000 0.500000 1.900000 1.000000 
foreach rbID $rbIDs { 
    # mol color ColorId $rbID
    mol modrep 0 $rbID
}

color Display Background white
color Name P green
color Name B green2
color change rgb green2 0.7 0.9 0.7

color Segname APRO cyan
color Segname BPRO blue
color Segname CPRO cyan3
color Segname DPRO blue3

light 0 off
display depthcue off
display backgroundgradient off
display shadows on
display ambientocclusion on
display aoambient 0.6
display aodirect 0.6

proc smooth {{s 3}} {
smoothReps $s
smoothRot $s
}
