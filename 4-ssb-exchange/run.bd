seed 289712415
timestep 2e-05
steps 10000000000
numberFluct 0                   # deprecated
rigidBodyGridGridPeriod 20

interparticleForce 1            # other values deprecated
fullLongRange 0                 # deprecated
temperature 291

outputPeriod 10000
## Energy doesn't actually get printed!
outputEnergyPeriod 10000
outputFormat dcd

## Infrequent domain decomposition because this kernel is still very slow
decompPeriod 10000
cutoff 35
pairlistDistance 25

origin -500.0 -500.0 -500.0
systemSize 1000 1000 1000

ParticleDynamicType Langevin
RigidBodyDynamicType Langevin
ParticleLangevinIntegrator BAOAB

particle B
num 70
mass 181.1
transDamping 1240 1240 1240
diffusion 43.5
gridFile potentials/plane.dx
gridFileScale 1.
rigidBodyPotential Bbead1
rigidBodyPotential Bbead2

particle B2
num 36
diffusion 43.5
mass 181.1
transDamping 1240 1240 1240
gridFile potentials/plane.dx
gridFileScale 1.
rigidBodyPotential B2bead1
rigidBodyPotential B2bead2

particle P
num 70
diffusion 43.5
mass 160.1
transDamping 1240 1240 1240
gridFile potentials/plane.dx
gridFileScale 1.
rigidBodyPotential Pbead1
rigidBodyPotential Pbead2

particle P2
num 36
diffusion 43.5
mass 160.1
transDamping 1240 1240 1240
gridFile potentials/plane.dx
gridFileScale 1.
rigidBodyPotential P2bead1
rigidBodyPotential P2bead2

inputRestraints restraint.txt

## Input coordinates
restartCoordinates dsdna18-ssdna70.0.restart
restartMomentum dsdna18-ssdna70.0.momentum.restart  

## Interaction potentials
tabulatedPotential  1
## The i@j@file syntax means particle type i will have NB interactions with particle type j using the potential in file
tabulatedFile 0@0@potentials/example-nb.B-B.dat
tabulatedFile 0@1@potentials/example-nb.B-B2.dat
tabulatedFile 0@2@potentials/example-nb.B-P.dat
tabulatedFile 0@3@potentials/example-nb.B-P2.dat
tabulatedFile 1@1@potentials/example-nb.B2-B2.dat
tabulatedFile 1@2@potentials/example-nb.B2-P.dat
tabulatedFile 1@3@potentials/example-nb.B2-P2.dat
tabulatedFile 2@2@potentials/example-nb.P-P.dat
tabulatedFile 2@3@potentials/example-nb.P-P2.dat
tabulatedFile 3@3@potentials/example-nb.P2-P2.dat
tabulatedBondFile tabPot/BPB.dat
tabulatedBondFile potentials/bond-1.000-8.410.dat
tabulatedBondFile potentials/bond-1.000-7.960.dat
tabulatedBondFile tabPot/BPP.dat
tabulatedBondFile tabPot/BBP.dat
tabulatedBondFile potentials/bond-10.000-7.835.dat
tabulatedAngleFile tabPot/p1p2b2.dat
tabulatedAngleFile potentials/angle-90.000-87.000.dat
tabulatedAngleFile potentials/angle-90.000-150.000.dat
tabulatedAngleFile potentials/angle-90.000-162.000.dat
tabulatedAngleFile tabPot/b1p2b2.dat
tabulatedAngleFile tabPot/b1p2p3.dat
tabulatedAngleFile tabPot/p1p2p3.dat
tabulatedDihedralFile tabPot/b1p2p3b3.dat
tabulatedDihedralFile tabPot/b1p1p2b2.dat
tabulatedDihedralFile tabPot/p0p1p2p3.dat
inputBonds potentials/example.bonds.txt
inputAngles potentials/example.angles.txt
inputDihedrals potentials/example.dihedrals.txt
inputExcludes potentials/example.exculsions.txt

rigidBody ssb1
num 1
mass 48856.4140625
inertia 19375326.0 18923212.0 13511158.0
inputRBCoordinates inputSSBCoords.txt
transDamping 779.396325558 757.577520666 735.633430273
rotDamping 3097.88531206 3029.1232551 3548.84746686

gridFile constrain potentials/plane.dx
densityGrid constrain den.dx
pmfScale constrain 1.

potentialGrid Pbead1 grid-P.dx
potentialGrid Bbead1 grid-B.dx
potentialGridScale Pbead1 0.5782764
potentialGridScale Bbead1 0.5782764
potentialGrid P2bead1 grid-P2.dx
potentialGrid B2bead1 grid-B2.dx
potentialGridScale P2bead1 0.5782764
potentialGridScale B2bead1 0.5782764

densityGrid vdw0 grids/1eyg.vdw0.den.dx
densityGrid vdw1 grids/1eyg.vdw1.den.dx
densityGrid vdw2 grids/1eyg.vdw2.den.dx
densityGrid elec grids/1eyg.charge.dx

rigidBody ssb2
num 1
mass 48856.4140625
inertia 19375326.0 18923212.0 13511158.0
inputRBCoordinates inputSSBCoords.txt
transDamping 779.396325558 757.577520666 735.633430273
rotDamping 3097.88531206 3029.1232551 3548.84746686

gridFile constrain potentials/plane.dx
densityGrid constrain den.dx
pmfScale constrain 1.

gridFile constrain1 sphere.dx
densityGrid constrain1 den.dx
pmfScale constrain1 1.

potentialGrid Pbead2 grid-P.dx
potentialGrid Bbead2 grid-B.dx
potentialGridScale Pbead2 0.5782764
potentialGridScale Bbead2 0.5782764
potentialGrid P2bead2 grid-P2.dx
potentialGrid B2bead2 grid-B2.dx
potentialGridScale P2bead2 0.5782764
potentialGridScale B2bead2 0.5782764

potentialGridScale vdw0 1.026964575
potentialGrid vdw0 grids/1eyg.vdw0.pot.dx
potentialGridScale vdw1 1.026964575
potentialGrid vdw1 grids/1eyg.vdw1.pot.dx
potentialGridScale vdw2 1.026964575
potentialGrid vdw2 grids/1eyg.vdw2.pot.dx
potentialGridScale elec 0.5782764
potentialGrid elec grids/1eyg.elec.dx
